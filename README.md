* 🎉DONE [ones 算子重构](https://gitee.com/mindspore/mindspore/pulls/21483)

* 🎉DONE [cos 算子重构](https://gitee.com/mindspore/mindspore/pulls/22280)
* 🎉DONE [abs 算子重构](https://gitee.com/mindspore/mindspore/pulls/22401)
* 🎉DONE [square 算子重构](https://gitee.com/mindspore/mindspore/pulls/22996)
* 🎉DONE [修复 abs 算子 empty tuple 输入 报错不正确](https://gitee.com/mindspore/mindspore/pulls/22739)
* 🎉DONE [移除无用代码](https://gitee.com/mindspore/mindspore/pulls/23938)
* 🎉DONE [tanh 算子重构](https://gitee.com/mindspore/mindspore/pulls/24015)

The above PRs are 🎉DONE.

Please note: the commit id is same even if  there are two different gitee ID(s).

I am very appreciate that i can work with nice mentor(s)/reviewer(s)/commiter(s):

@ljl0711 @simon_wu🐱 @lianliguang🦇 @liangchenghui @ginfung

This gi-tbase commit log messages are awfully messy🐍, next time will i submit more officially/standard.

🎇Again:thanks to this activity and all the relative people who host this event.
